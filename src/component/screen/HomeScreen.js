import {
  SafeAreaView,
  Text,
  ActivityIndicator,
  StatusBar,
  RefreshControl,
  ScrollView,
  FlatList,
  View,
  ToastAndroid,
  Platform,
  TouchableWithoutFeedback,
} from 'react-native';
import React, {Component} from 'react';

import * as Api from '../../api/Api';
import ListCard, {HEIGHT} from '../basic/ListCard';
import ListFooter from '../basic/ListFooter';
import ColorFilter from '../basic/ColorFilter';

import {SearchBar} from 'react-native-elements';
import {Colors} from '../../GlobalConstants';
import {styles} from './style';
import Icon from 'react-native-vector-icons/MaterialIcons';

const SEARCH_DELAY_MS = 1000;
const SCROLL_UP_DELAY_MS = 250;
const START_PAGE = 1;

export default class HomeScreen extends Component {
  static navigationOptions = {
    title: 'Magic The Gathering',
    headerStyle: styles.headerStyle,
    headerTintColor: Colors.white,
    headerTitleStyle: styles.headerTitleStyle,
  };

  constructor(props) {
    super(props);
    this.state = {
      page: START_PAGE,
      cards: null,
      loading: false,
      errorMessage: undefined,
      search: '',
      searchingWithFilter: '',
      showFilter: false,
      colors: {
        white: true,
        blue: true,
        black: true,
        red: true,
        green: true,
      },
    };
  }

  componentDidMount() {
    this._fetchCards(false);
  }

  _fetchCards = async hasNewColors => {
    if (this.state.loading) {
      console.debug('HomeScreen', 'Already fetching cards');
      return;
    }
    try {
      let newSearch = this._shouldResetThePage(this.state.search) || hasNewColors;
      console.debug(
        'HomeScreen',
        `Searching with name '${this.state.search}' ${newSearch ? ', is new search' : ''}`,
      );
      this.setState({loading: true});
      let data = await Api.getData(
        newSearch ? START_PAGE : this.state.page,
        Api.VARIABLE_CONTAINS_IMAGE,
        this.state.search,
        this.state.colors,
      );
      this.setState(
        {
          cards: this.state.cards == null || newSearch ? data : [...this.state.cards, ...data],
          loading: false,
          errorMessage: undefined,
          page: Number(newSearch ? START_PAGE : this.state.page) + 1,
          searchingWithFilter: this.state.search,
        },
        () => {
          if (newSearch) {
            setTimeout(() => {
              this.list.scrollToOffset({animated: true, offset: 0});
            }, SCROLL_UP_DELAY_MS);
          }
        },
      );
    } catch (error) {
      if (Platform.OS === 'android') {
        ToastAndroid.show('Could not contact server', ToastAndroid.LONG);
      }
      this.setState({
        loading: false,
        errorMessage: 'Something went wrong, pull to refresh',
      });
    }
  };

  render() {
    let content;
    if (this.state.cards == null) {
      // Code when there is no data yet
      if (this.state.loading) {
        content = this._renderLoading();
      } else {
        // Loading has finished but no cards, must mean an error
        content = this._renderError();
      }
    } else {
      // Code when there is data
      content = this._renderCards();
    }
    return (
      <SafeAreaView style={styles.homeContainer}>
        <StatusBar barStyle="light-content" backgroundColor={Colors.primaryColor} />
        {content}
      </SafeAreaView>
    );
  }

  _renderLoading = () => {
    console.debug('HomeScreen', 'Render loading');
    return <ActivityIndicator size="large" color={Colors.black} />;
  };

  _renderError = () => {
    console.debug('HomeScreen', 'Render error');
    return (
      <ScrollView
        contentContainerStyle={styles.errorContainer}
        refreshControl={
          <RefreshControl
            refreshing={this.state.loading}
            onRefresh={() => {
              this._fetchCards(false);
            }}
          />
        }>
        <Text>{this.state.errorMessage}</Text>
      </ScrollView>
    );
  };

  _renderCards = () => {
    console.debug(
      'HomeScreen',
      `Render ${this.state.cards.length} cards, searching for '${this.state.search}'`,
    );

    return (
      <View style={styles.dataContainer}>
        <View style={{flexDirection: 'row'}}>
          <SearchBar
            platform="android"
            placeholder="Search..."
            value={this.state.search}
            onChangeText={this._onSearch}
            underlineColorAndroid={Colors.primaryColor}
            showLoading={this.state.loading}
            disabled={this.state.loading}
            loadingProps={styles.searchBarActivityStyle}
            containerStyle={{flex: 1}}
          />

          <TouchableWithoutFeedback
            onPress={() => {
              if (!this.state.loading) {
                this.setState({showFilter: !this.state.showFilter});
              }
            }}>
            <View
              style={{
                height: 56,
                width: 56,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Icon
                name={'filter-list'}
                size={24}
                color={this.state.loading ? Colors.grey : Colors.black}
              />
            </View>
          </TouchableWithoutFeedback>
        </View>
        <ColorFilter
          key={JSON.stringify(this.state.colors)}
          show={this.state.showFilter && !this.state.loading}
          colors={this.state.colors}
          submit={newColors => {
            if (JSON.stringify(this.state.colors) == JSON.stringify(newColors)) {
              this.setState({
                showFilter: false,
              });
              console.debug('HomeScreen', `No new colors, just hiding filter`);
            } else {
              console.debug('HomeScreen', `New colors, needs to restart request`);
              this.setState(
                {
                  showFilter: false,
                  colors: newColors,
                },
                () => {
                  this._fetchCards(true);
                },
              );
            }
          }}
        />

        <FlatList
          ref={ref => {
            this.list = ref;
          }}
          initialNumToRender={Api.DEFAULT_PAGE_SIZE}
          data={this.state.cards}
          keyExtractor={card => card.id + card.name}
          getItemLayout={(data, index) => ({
            length: HEIGHT,
            offset: HEIGHT * index,
            index,
          })}
          renderItem={({item}) => (
            <ListCard
              onPress={url => {
                this.props.navigation.navigate('ImageModal', {url: url});
              }}
              card={item}
            />
          )}
          ListFooterComponent={() =>
            this.state.cards && this.state.cards.length > 0 ? (
              <ListFooter
                onPress={() => {
                  this._fetchCards(false);
                }}
                loading={this.state.loading}
              />
            ) : null
          }
        />
      </View>
    );
  };

  _onSearch = value => {
    this.setState({
      search: value,
    });
    if (this.timer) {
      clearTimeout(this.timer);
    }
    this.timer = setTimeout(() => {
      this.timer = null;
      console.debug('HomeScreen', `Start searching for name`);
      this._fetchCards(false);
    }, SEARCH_DELAY_MS);
  };

  _shouldResetThePage = () => {
    return this.state.search != this.state.searchingWithFilter;
  };
}
