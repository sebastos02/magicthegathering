import {TouchableWithoutFeedback, ActivityIndicator, View} from 'react-native';
import FastImage from 'react-native-fast-image';
import React, {Component} from 'react';
import {Colors} from '../../GlobalConstants';
import {styles} from './style';

const DEFAULT_WIDTH = 223;
const DEFAULT_HEIGHT = 310;
export default class ImageModal extends Component {
  constructor(props) {
    super(props);
    this.state = {loading: true};
  }
  render() {
    const {params} = this.props.navigation.state;
    console.log('Showing ' + params.url);

    return (
      <TouchableWithoutFeedback onPress={() => this.props.navigation.goBack()}>
        <View style={styles.imageModalBg}>
          {this.state.loading && (
            <ActivityIndicator style={styles.imageLoader} color={Colors.white} size="large" />
          )}
          <FastImage
            style={{width: DEFAULT_WIDTH, height: DEFAULT_HEIGHT}}
            source={{
              uri: params.url,
            }}
            onLoadEnd={() => {
              this.setState({
                loading: false,
              });
            }}
            onError={() => {
              this.props.navigation.goBack();
            }}
          />
        </View>
      </TouchableWithoutFeedback>
    );
  }
}
