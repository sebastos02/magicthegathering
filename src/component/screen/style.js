import {StyleSheet} from 'react-native';
import {Colors} from '../../GlobalConstants';

export const styles = StyleSheet.create({
  headerStyle: {
    backgroundColor: Colors.primaryColor,
  },
  headerTitleStyle: {
    fontWeight: 'bold',
  },
  homeContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  errorContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  dataContainer: {
    width: '100%',
    height: '100%',
  },
  searchBarActivityStyle: {
    color: Colors.black,
  },
  imageModalBg: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.blackTransparant,
  },
  imageLoader: {position: 'absolute', alignSelf: 'center'},
});
