import {StyleSheet} from 'react-native';
import {Colors} from '../../GlobalConstants';
import {WIDTH, HEIGHT} from './ListCard';

export const styles = StyleSheet.create({
  cardContainer: {
    backgroundColor: Colors.whiteGrey,
    borderRadius: 4,
  },
  thumbnail: {
    width: 50,
    height: 50,
  },
  cardInfoContainer: {
    flex: 1,
    marginLeft: 8,
  },
  cardInfoTop: {
    flexDirection: 'row',
    marginBottom: 4,
  },
  cardTitle: {
    color: Colors.black,
    fontWeight: 'bold',
    flex: 1,
  },
  cardSetName: {
    color: Colors.black,
  },
  cardInfoBotom: {
    flexDirection: 'row',
  },
  cardType: {
    color: Colors.black,
    flex: 1,
  },
  circleColor: {
    marginLeft: 2,
    width: 20,
    height: 20,
    borderRadius: 20,
  },
  filterOption: {
    height: 42,
    flex: 1,
    margin: 4,
  },
  colorFilterContainer: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    height: 50,
    backgroundColor: Colors.white,
    elevation: 4,
    flexDirection: 'row',
  },
  filterOptionsRow: {
    flex: 1,
    flexDirection: 'row',
  },
  filterSubmit: {
    height: 50,
    width: 56,
    justifyContent: 'center',
    alignItems: 'center',
  },
  filterInclude: {
    position: 'absolute',
    width: 16,
    height: 16,
    bottom: 0,
    right: 0,
    backgroundColor: Colors.primaryColor,
    alignItems: 'center',
    justifyContent: 'center',
  },
  filterNotInclude: {
    position: 'absolute',
    width: 16,
    height: 16,
    bottom: 0,
    right: 0,
    backgroundColor: Colors.white,
    borderWidth: 1,
    borderColor: Colors.primaryColor,
  },
});
