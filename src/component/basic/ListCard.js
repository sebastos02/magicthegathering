import {Card} from 'react-native-elements';
import {Text, View, Dimensions, TouchableWithoutFeedback} from 'react-native';
import React, {Component} from 'react';
import FastImage from 'react-native-fast-image';
import {Colors} from '../../GlobalConstants';
import {styles} from './style';

export const HEIGHT = 80;
export const WIDTH = Dimensions.get('screen').width - 32;

export default class ListCard extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <TouchableWithoutFeedback
        onPress={() => {
          this.props.onPress(this.props.card.imageUrl);
        }}>
        <Card containerStyle={[styles.cardContainer, {width: WIDTH, height: HEIGHT}]}>
          <View style={{flexDirection: 'row'}}>
            <FastImage
              style={styles.thumbnail}
              resizeMode="contain"
              source={{
                uri: this.props.card.imageUrl,
              }}
            />
            <View style={styles.cardInfoContainer}>
              <View style={styles.cardInfoTop}>
                <Text numberOfLines={1} style={styles.cardTitle}>
                  {this.props.card.name}
                </Text>
                <Text style={styles.cardSetName}>{this.props.card.setName}</Text>
              </View>
              <View style={styles.cardInfoBotom}>
                <Text numberOfLines={1} style={styles.cardType}>
                  {this.props.card.type}
                </Text>
                {this.props.card.colors.map(color => {
                  color = color.toLowerCase();
                  return (
                    <View
                      style={[
                        styles.circleColor,
                        {
                          backgroundColor: color,
                          borderColor: color == 'white' ? Colors.black : undefined,
                          borderWidth: color == 'white' ? 1 : undefined,
                        },
                      ]}
                    />
                  );
                })}
              </View>
            </View>
          </View>
        </Card>
      </TouchableWithoutFeedback>
    );
  }
}
