import {Text, View, TouchableWithoutFeedback, ActivityIndicator} from 'react-native';
import React, {Component} from 'react';
import {Colors} from '../../GlobalConstants';

export default class ListFooter extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <View
        style={{
          flex: 1,
          alignItems: 'center',
        }}>
        <TouchableWithoutFeedback onPress={this.props.onPress}>
          <View
            style={{
              backgroundColor: Colors.primaryColor,
              padding: 8,
              borderRadius: 4,
              elevation: 4,
              margin: 8,
              height: 36,
              width: 100,
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            {this.props.loading ? (
              <ActivityIndicator color={Colors.white} />
            ) : (
              <Text style={{color: Colors.white}}>MORE</Text>
            )}
          </View>
        </TouchableWithoutFeedback>
      </View>
    );
  }
}
