import {Text, View, TouchableWithoutFeedback} from 'react-native';
import React, {Component} from 'react';
import {Colors} from '../../GlobalConstants';
import Icon from 'react-native-vector-icons/MaterialIcons';
import {styles} from './style';

export default class ColorFilter extends Component {
  constructor(props) {
    super(props);
    this.state = {
      updateColors: {...props.colors},
    };
  }

  render() {
    if (this.props.show) {
      let colors = [];
      Object.keys(this.state.updateColors).forEach((key, index) => {
        colors.push(
          <TouchableWithoutFeedback
            onPress={() => {
              let newColors = this.state.updateColors;
              newColors[key] = !newColors[key];
              this.setState({
                updateColors: newColors,
              });
            }}>
            <View
              key={key}
              style={[
                styles.filterOption,
                {
                  backgroundColor: key,
                  borderColor: key == 'white' ? 'black' : undefined,
                  borderWidth: key == 'white' ? 1 : 0,
                },
              ]}>
              {this.state.updateColors[key] ? this._included() : this._notIncluded()}
            </View>
          </TouchableWithoutFeedback>,
        );
      });
      return (
        <View style={styles.colorFilterContainer}>
          <View style={styles.filterOptionsRow}>{colors}</View>
          <TouchableWithoutFeedback onPress={this._onSubmit}>
            <View style={styles.filterSubmit}>
              <Icon name={'check'} size={24} color={Colors.black} />
            </View>
          </TouchableWithoutFeedback>
        </View>
      );
    } else {
      return null;
    }
  }

  _onSubmit = () => {
    this.props.submit(this.state.updateColors);
  };

  _included = () => {
    return (
      <View style={styles.filterInclude}>
        <Icon name="check" size={14} color={Colors.white} />
      </View>
    );
  };

  _notIncluded = () => {
    return <View style={styles.filterNotInclude} />;
  };
}
