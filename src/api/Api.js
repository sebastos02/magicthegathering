const BASE_URL = 'https://api.magicthegathering.io/v1/cards?';
const AND = '&';
const PARAM_PAGE = 'page=';
const PARAM_PAGE_SIZE = 'pageSize=';
const PARAM_CONTAINS = 'contains=';
const PARAM_NAME = 'name=';
const PARAM_COLORS = 'colors=';

export const DEFAULT_PAGE_SIZE = 20;
export const VARIABLE_CONTAINS_IMAGE = 'imageUrl';
export async function getData(page, contains, search = null, colors) {
  console.debug('Api', 'Trying to retrieve cards');
  try {
    let colorsRequested = _getColorsRequested(colors);
    let request =
      BASE_URL +
      PARAM_PAGE +
      page +
      AND +
      PARAM_PAGE_SIZE +
      DEFAULT_PAGE_SIZE +
      (contains ? AND + PARAM_CONTAINS + contains : '') +
      (search ? AND + PARAM_NAME + search : '') +
      (colorsRequested != '' ? AND + PARAM_COLORS + colorsRequested : '');
    console.debug('Api', `Request = ${request}`);
    let response = await fetch(request);
    let object = await response.json();
    console.debug('Api', `Retrieved ${object.cards ? object.cards.length : 0} new cards`);

    // There is a need to filter because the param 'contains' doesn't always
    // work as described in the API
    if (object.cards.length > 0) {
      return object.cards.filter(card => card.imageUrl);
    } else {
      return object.cards;
    }
  } catch (err) {
    console.log(err);
    throw `Something went wrong with the request: ${err}`;
  }
}

const _getColorsRequested = colors => {
  let colorsRequested = '';
  Object.keys(colors).forEach((key, index) => {
    if (colors[key]) {
      colorsRequested += key + '|';
    }
  });
  if (colorsRequested != '') {
    colorsRequested = colorsRequested.substring(0, colorsRequested.length - 1);
  }
  return colorsRequested;
};
