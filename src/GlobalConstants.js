export const Colors = {
  white: '#FFFFFF',
  black: '#000000',
  primaryColor: '#DD2C00',
  whiteGrey: '#FAFAFA',
  blackTransparant: '#00000080',
  grey: '#888888',
};
