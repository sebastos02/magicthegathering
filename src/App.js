import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';

import HomeScreen from './component/screen/HomeScreen';
import ImageModal from './component/screen/ImageModal';

const AppNavigator = createStackNavigator({
  Home: {
    screen: HomeScreen,
  },
});

const RootStack = createStackNavigator(
  {
    Main: {
      screen: AppNavigator,
    },
    ImageModal: {
      screen: ImageModal,
      navigationOptions: {
        header: null,
      },
    },
  },
  {
    mode: 'modal',
    headerMode: 'none',
    transparentCard: true,
    cardStyle: {opacity: 1},
  },
);

export default createAppContainer(RootStack);
