# Magic The Gathering

There is an **app-release.apk** in the root of the project in case you want to test the release build.

I have not tested the iOS build due to lack of hardware, so I cannot promise it will run without any issues on an iPhone.

## Installation

The first step is to include **local.properties** file inside the **/android** folder. In this file you must have the path to your android SDK. You can copy this file from another Android project, or simply open the Android project inside Android Studio and it should automatically add this file.

The path is usually something like:

*sdk.dir=C\:\\\Users\\\myuser\\\AppData\\\Local\\\Android\\\sdk*

When that is done, just run the following commands in the root of the project.
```bash
npm install
react-native run-android
```